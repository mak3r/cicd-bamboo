Add-Type -AssemblyName System.IO.Compression.FileSystem

$url = "https://www.atlassian.com/software/bamboo/downloads/binary/atlassian-bamboo-6.2.1-windows-x64.exe"
$output = "C:\vagrant\windows\bamboo.exe"
Write-Output "Downloading Bamboo, please wait..."
(New-Object System.Net.WebClient).DownloadFile($url, $output)
Write-Output "Download complete"
