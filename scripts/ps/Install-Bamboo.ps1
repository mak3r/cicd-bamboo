Write-Output "Running Bamboo Installer"

$bamboo = "C:\vagrant\windows\bamboo.exe "
$args = "-varfile C:\vagrant\windows\bamboo.install.txt -q -c"

try {
    Write-Host 'Installing Bamboo'
    $proc1 = Start-Process -FilePath "$bamboo" -ArgumentList "$args" -Wait -PassThru
    $proc1.waitForExit()
    Write-Host 'Installation Done.'
} catch [exception] {
    write-host '$_ is' $_
    write-host '$_.GetType().FullName is' $_.GetType().FullName
    write-host '$_.Exception is' $_.Exception
    write-host '$_.Exception.GetType().FullName is' $_.Exception.GetType().FullName
    write-host '$_.Exception.Message is' $_.Exception.Message
}
